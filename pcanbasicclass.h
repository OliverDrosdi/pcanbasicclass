#ifndef PCANBASICCLASS_H
#define PCANBASICCLASS_H
#include <QObject>
#include <QDebug>
#include <functional>
#include <windows.h>
#include <exception>
#include <future>
#include <deque>
#include <set>
#include "pcanbasicclass_global.h"


class PCANBASICCLASSSHARED_EXPORT msgBase
{
public:
    msgBase(DWORD,int,char*);
    msgBase();
    ~msgBase();
    msgBase& operator =(const msgBase&);
    DWORD m_msgID;
    int m_msgLength;
    char* m_payload=nullptr;
    std::array<char,8> getPayLoadAsArray();
    void setPayload(std::array<char,8>);
};

PCANBASICCLASSSHARED_EXPORT QDebug operator<<(QDebug dbg, const msgBase &obj);

PCANBASICCLASSSHARED_EXPORT QDebug operator<<(QDebug dbg, const std::array<char,8>&obj);

class PCANBASICCLASSSHARED_EXPORT PCANBasicClass: public QObject
{
    Q_OBJECT
public:
    PCANBasicClass(QString,QObject* parent=0);
    virtual ~PCANBasicClass();
    bool start();
    void stop();
    bool m_stopper=false;
public:
    std::array<char,8> getMsgById(DWORD);
public:
    void pumpOutMsg(msgBase);
private:
    void configure();
    QObject *m_device=nullptr;
    std::future<bool>* m_finished;
    bool runner();
    QString m_usbId;
signals:
    void sendMsg(DWORD,int,std::array<char,8>);
public slots:
    void slotsendMsg(DWORD,int,std::array<char,8>);
    void receiveAll();
};

#endif // PCANBASICCLASS_H
